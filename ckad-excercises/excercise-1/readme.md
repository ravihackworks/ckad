# Excercise - 1

```sh
Create a job called whalesay with image docker/whalesay and command "cowsay I am going to ace CKAD!".  Below specification should be used - 
  - completions: 10
  - backoffLimit: 6
  - restartPolicy: Never
```