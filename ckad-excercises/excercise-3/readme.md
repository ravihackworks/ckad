# Excercise - 3

```sh
Create a deployment called my-webapp with image: nginx, label tier:frontend and 2 replicas. Expose the deployment as a NodePort service with name front-end-service , port: 80 and NodePort: 30083
```

# Solution 
  1. Create deployment 
     ```sh
     - kubectl create deployment my-webapp --image=nginx --replicas=2
	 - kubectl label deployment my-webapp tier=frontend
	 ```
  2. Expose deployment for node port 
     ```sh 
      - kubectl expose deployment my-webapp --type=NodePort --name=front-end-service --port=80 --dry-run -o yaml > svc.yaml
	  - Add property for nodePort: 30083
	  ```
	