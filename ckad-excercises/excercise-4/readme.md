# Excercise - 4

```sh
Apply a label app_type=beta to node node02. Create a new deployment called beta-apps with image:nginx and replicas:3. Set Node Affinity to the deployment to place the PODs on node02 only
```

# Solution 
  1. Set label to cluster node 
     ```sh
     - kubectl label nodes node02 app_type=beta
	 ```
  2. Create deployment
     ```sh 
      - kubectl create deployment beta-apps --image=nginx --replicas=3 --dry-run -o yaml > svc.yaml
	  - Add property for tolerations
	  ```